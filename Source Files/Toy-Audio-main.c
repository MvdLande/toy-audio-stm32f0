#include <stm32f0xx_gpio.h>
#include <stm32f0xx_rcc.h>
#include <stm32f0xx_tim.h>
#include <stm32f0xx_misc.h>
#include <stm32f0xx.h>
#include "WAVplayer.h"
#include "sFlash.h"
#include "usart.h"
#include "common.h"
#include "Terminal.h"
//#include "ok-sound.h"
#include "stm32f0xx_pwr.h"
#include "stm32f0xx_dbgmcu.h"
#include "stm32f0xx_rtc.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_syscfg.h"
#include "stm32f0xx_exti.h"

uint16_t CCR4_Val = 0x80;
uint8_t PWMvalue, Prev_WAVdata;
uint8_t PWMscaler = 3;
uint8_t ITcounter = 0;
uint32_t WAVindex=0;
uint32_t counter = 0;
uint32_t shake_counter = 0;
uint16_t x = 0;
//uint32_t WAV_Filesize;
//uint32_t WAV_File_Index;

// deplop, 
// use 0.5s (4000 counts) to ramp pwm op to mid value (128)
#define  WaitCount 4000/128
			  

extern void TIM3_IRQHandler(); // reference to TIM3 interrupt vector

void InitTIM3GPIO(void);
void DeInitTIM3GPIO(void);
void InitTIM3(void);
void InitClocks(void);
void SelectHSI(void);
void Sleep(void);
void InitAudioGPIO(void);
void InitButtonGPIO(void);
void InitWkupGPIO(void);
void InitEXTI(void);
void DisableShake(void);

//Define time keeping variables
volatile uint32_t Milliseconds = 0, Seconds = 0;
typedef enum {
	start1 = 0, start2, playing1, played1, playing2, played2, waitingfor1, waitingfor2
} StateTypeDef;

void SysTick_Handler(void) {
	Milliseconds++; //Increment millisecond variable
	if (Milliseconds % 100 == 999) { //If 100 x 10 milliseconds have passed, increment seconds
		Seconds++;
	}
}


void Delay(uint32_t MilS) 
{
	Systick_Enable();		//start systick timer
	// systick is configured for a 10ms delay 
	uint32_t MSStart = Milliseconds;
	if (MilS > 9)
	{
		while ((Milliseconds - MSStart) < MilS / 10) 
			Sleep(); //wait for a (systick) interrupt
			//asm("nop");
	}
	else
	{
		while ((Milliseconds - MSStart) < 1) 
			Sleep(); //wait for a (systick) interrupt
			//asm("nop");
	}
	Systick_Disable(); //stop systick time
}

//Delay function for second delay 
void DelaySec(uint32_t S)
{
	uint32_t Ss = Seconds;
	Systick_Enable();		//start systick timer
	while ((Seconds - Ss) < S)
		Sleep(); //wait for a (systick) interrupt
		//asm("nop");
	Systick_Disable(); //stop systick time
}

WAV_Player_File_8bitMono aWAV_File;	// global WAV file struct
uint8_t WAVdata;
uint8_t *WAVdataPointer = &WAVdata;
uint8_t WAV_DataPending = 0;

void InitClocks(void) {
    /*
    HSI: 8 MHz
    PLL: RCC_PLLSource_HSI_Div2 * 4 => 16 MHz
    */
	RCC_DeInit(); //free PF0 and PF1
	RCC_PLLConfig(RCC_PLLSource_HSI_Div2, 8);
	RCC_PLLCmd(ENABLE);

	    // Wait
	while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) continue;

	    // Use PLL
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

	while (RCC_GetSYSCLKSource() != 0x08) continue;	// 0x08: PLL used as system clock
	
	//RCC_PREDIV1Config(RCC_PREDIV1_Div1);
	RCC_HCLKConfig(RCC_SYSCLK_Div1); // 16MHz
	RCC_PCLKConfig(RCC_SYSCLK_Div1); // 16MHz
	
	//FLASH_SetLatency(FLASH_Latency_1);
	//FLASH_PrefetchBufferCmd(ENABLE);

}
void SelectHSI(void)
{
	RCC_DeInit(); //free PF0 and PF1
	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);
	while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET)
		;
	RCC_PREDIV1Config(RCC_PREDIV1_Div1);
	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	RCC_PCLKConfig(RCC_SYSCLK_Div1);
}

void Stop(void)
{
	sFLASH_PowerDown();
	//DisableShake();
	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	//GPIO_ResetBits(GPIOA, GPIO_Pin_1);  //disable audio amp
	//GPIO_SetBits(GPIOF, GPIO_Pin_0);  //deselect SPI Flash
	DeInitTIM3GPIO();
	while ((GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == Bit_SET))
		;  //wait
	Delay(500); // wait for button to debounce
	Delay(10);
	EXTI->PR = 0; // clear pending events
	PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFE);
	PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFE);
	EXTI->PR &= ~EXTI_Line0; // clear pending event
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, DISABLE);
	SelectHSI(); //8MHz clock	init clock after stop mode
	sFLASH_ReleasePowerDown();
	InitEXTI(); // Enable shake interrupt
	PWMscaler = 3; //normal speed

}

void InitEXTI(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
	//PA0 = wakeup button
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//EXTI->PR = 0; // clear pending events
	
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
	

	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Event; // EXTI_Mode_Interrupt; // 
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	
	EXTI_Init(&EXTI_InitStructure);

	//PA10 = shake/vibration sensor 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
/*	
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource4);
	

	EXTI_InitStructure.EXTI_Line = EXTI_Line10;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	
	EXTI_Init(&EXTI_InitStructure);
	
	NVIC_InitTypeDef NVIC_InitStructure;
	
	// Enable and set the EXTIO Interrupt to the lowest priority
	NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	
	NVIC_Init(&NVIC_InitStructure);
*/
	
}
void EXTI4_15_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line10) != RESET)
	{
		shake_counter++; //increment shake counter

		/* Clear the EXTI line 10 pending bit */
		EXTI_ClearITPendingBit(EXTI_Line10);
	}
}

void DisableShake(void)
{
	EXTI_InitTypeDef EXTI_InitStructure;
	
	EXTI_InitStructure.EXTI_Line = EXTI_Line10;
	EXTI_InitStructure.EXTI_LineCmd = DISABLE;
	
	EXTI_Init(&EXTI_InitStructure);
	/* Clear the EXTI line 10 pending bit */
	EXTI_ClearITPendingBit(EXTI_Line10);
}

void Sleep(void)
{
	PWR_EnterSleepMode(PWR_SLEEPEntry_WFI | PWR_SLEEPEntry_WFE);
}

void Standby(void)
{
	uint32_t i;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE); 
	InitButtonGPIO();
	sFLASH_PowerDown();
	//DeInitTIM3GPIO();
	while ((GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == Bit_SET))
		;  //wait
	SysTick->CTRL  |= SysTick_CTRL_ENABLE_Msk; // start systick timer
	Delay(200); // wait for button to debounce
		// disable systick timer
	SysTick->CTRL  &= ~SysTick_CTRL_ENABLE_Msk; //stop systick timer
	
	//RCC_APB1PeriphClockCmd(RCC_APB2Periph_DBGMCU, ENABLE); 
	
	//PWR_ClearFlag(PWR_FLAG_SB);
	// Check if the StandBy flag is cleared 
	//if (PWR_GetFlagStatus(PWR_FLAG_SB) != RESET)
	//{
	//	while (1)
	//		;
	//}
	
	/*
	// Enable the LSI OSC 
	RCC_LSICmd(ENABLE);
	// Wait till LSI is ready 
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
	{}
	*/
	//InitWkupGPIO(); // not needed
	//RTC_DeInit();

	//PWR_WakeUpPinCmd(PWR_WakeUpPin_1 | PWR_WakeUpPin_2, DISABLE);
	//DBGMCU_Config(DBGMCU_STANDBY, ENABLE); //enable debugging during standy mode
	PWR_ClearFlag(PWR_FLAG_WU | PWR_FLAG_SB);
	//PWR->CR |= PWR_CR_CWUF;
	//RTC_ClearFlag(RTC_FLAG_ALRAF);
	PWR_WakeUpPinCmd(PWR_WakeUpPin_1, ENABLE);
	//for (i = 0; i++; i < 5000)
	//	;
	PWR_EnterSTANDBYMode();
	// the device will be reset when exiting standby mode
	//InitButtonGPIO();
}



	

int main()
{
	  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f0xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f0xx.c file
     */
	int32_t i, j, temp, s = 0;
	uint8_t Shaker_State=0, not_shaked=0;
	StateTypeDef  State;
	//InitClocks();
	InitAudioGPIO();
	InitButtonGPIO();
	SelectHSI(); //8MHz clock
	//Systick_Enable();
	InitUSART1();
	// Clear the input path 
	USART_RequestCmd(USART1, USART_Request_SBKRQ, ENABLE);
	//Delay(1000);
	//InitTIM3();
	
	//sFLASH_BitBangInit();
	
	
	//RTC_RefClockCmd(ENABLE); // The RTC registers are used to store the song status
	  /* Enable the PWR clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	  /* Disable the write protection for RTC registers */
	  /* Allow access to RTC */
	PWR_BackupAccessCmd(ENABLE);
	/*
    //Enter RTC Init mode
	RTC->ISR = 0; 
	RTC->ISR |= RTC_ISR_INIT; 
	while ((RTC->ISR & RTC_ISR_INITF) == 0)
		;
	// Set 24-h clock
	RTC->CR |= RTC_CR_FMT; 
*/

	/*
	RCC_LSICmd(ENABLE);
	// Wait till LSI is ready 
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
	{
	}
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
	RCC_RTCCLKCmd(ENABLE);
	RTC_WriteProtectionCmd(DISABLE);
	RTC_EnterInitMode();
	// Wait for RTC APB registers synchronisation 
	//RTC_WaitForSynchro();
	*/
	Delay(100);
	//RTC->CR |= RTC_CR_FMT;
	//Delay(1);
	i = 0;
	if ((RTC->CR & RTC_CR_FMT) == RTC_CR_FMT)
		State = waitingfor1; // Enlish song
	else
		State = waitingfor2; // Dutch song
	//RCC_RTCCLKCmd(DISABLE);
	Systick_Disable();
	InitEXTI(); // enable button event and shake sensor
	for (;;)
	{
		// check for Serial data
		if (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) != RESET)
		{
			TIM_Cmd(TIM3, DISABLE);				//Stop PWM generator
			DeInitTIM3GPIO();
	
			// Clear the input path 
			USART_RequestCmd(USART1, USART_Request_SBKRQ, ENABLE);
			//load music to spi flash
			Main_Menu();
			// start song again
			if ((RTC->CR & RTC_CR_FMT) == RTC_CR_FMT)
				State = waitingfor1; // Enlish song
			else
				State = waitingfor2; // Dutch song
			Delay(100);
			// Clear the input path 
			USART_RequestCmd(USART1, USART_Request_SBKRQ, ENABLE);
		}
	
		// sample shaker
		if ((GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_10 == Bit_SET)))
		{
			if (Shaker_State == 0)
			{
				Shaker_State = 1;
				shake_counter++;
			}
			else
				Shaker_State = 0;				
		}

		if (WAV_DataPending == 1)
		{
			// shake sensor
			// IF PWMscaler = 3 --> 8KHz Audio
			// IF PWMscaler = 1 --> 16KHz Audio
			// If more then 2 shakes/second then increase playing speed
			// sample shaker input


			s++;
			if (PWMscaler == 3)
			{
				if (s > 8000)
				{
					if (shake_counter > 2)
					{
						PWMscaler = 1; // speedup!
						shake_counter = 0;
					}
					s = 0; 
				}
			}
			if (PWMscaler == 1)
			{
				if (s > 16000)
				{
					if (shake_counter > 2)
					{
						//if (not_shaked>0)
							not_shaked = 0;
						shake_counter = 0;
					}
					else
					{

						if (not_shaked >= 1)
								PWMscaler = 3; // normal speed.
						not_shaked++;
					}
						
					s = 0; 
				}
			}
			
			if (State == playing1 || State == playing2)
			{
				temp = WAV_Player_GetByte(&aWAV_File, &WAVdata);
				if (temp == 1)
				{
					WAV_DataPending = 0;
				}
				else
				{
					WAV_DataPending = 0;
					if (State == playing1)
						State = played1;
					if (State == playing2)
						State = played2;
				}	
				if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == Bit_SET)
				{
					if (i == 10000)	// 1.25 seconds
					{
						//stop playing
						if (State == playing1)
							State = played1;
						if (State == playing2)
							State = played2;
					}
					if (i == 750)	// 
					{
						//next
						if (State == playing1)
							State = start2;
						if (State == playing2)
							State = start1;
					}
					i++;
				}
				else
				{	
					if (j < 1000)
						j++;
					else
					{
						i = 0;
						j = 0;
					}

				}
			}
		}
	
		if (State == played1 || State == played2)
		{
			//uint32_t temp;
			GPIO_ResetBits(GPIOA, GPIO_Pin_1); //disable Audio Amp
			WAV_DataPending = 0;  
			//RTC_EnterInitMode();
			//temp = (RTC->CR)&(~(RTC_CR_WUCKSEL_0 | RTC_CR_WUCKSEL_1));
			if (State == played1)
			{
				State = waitingfor1; // next button selects next song
				//RTC->CR &= ~RTC_CR_FMT;				
			}

			if (State == played2)
			{
				State = waitingfor2; // next button selects next song
				//RTC->CR |= RTC_CR_FMT;				
			};
			Stop();
		}
		
		if (State == waitingfor1 || State == waitingfor2)
		{
			//Stop();	// goto stop mode, wait for trigger event
			if (State == waitingfor1)
				State = start1;
			if (State == waitingfor2)
				State = start2;	
		}
		if (State == start1 || State == start2)
		{
			if (State == start1)
			{
				WAV_FILE_ADDRESS = WAV_FILE_ADDRESS1;
				State = playing1;
			}
			if (State == start2)
			{
				WAV_FILE_ADDRESS = WAV_FILE_ADDRESS2;
				State = playing2;
			}			  
			WAV_DataPending = 1;  
			aWAV_File.FileStartAddress = WAV_FILE_ADDRESS;
			Start_WAV_Player(&aWAV_File);
		}
		
	Sleep();
	}
}



void InitButtonGPIO(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD; 
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void InitWkupGPIO(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD; 
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	// PA0 = WKUP1
	// GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_0); // select WKUP
}


void InitAudioGPIO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; 
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_ResetBits(GPIOA, GPIO_Pin_1);  //disable audio amp

	
}

void InitTIM3GPIO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	//GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; //TIM3
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; 
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource1, GPIO_AF_1);
	// Turn Speaker on
	//GPIO_SetBits(GPIOA, GPIO_Pin_1);
	
}
void DeInitTIM3GPIO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; 
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	// Turn Speaker off
	GPIO_ResetBits(GPIOA, GPIO_Pin_1);

}
			  

void InitTIM3(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	TIM_DeInit(TIM3);
	
	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_InternalClockConfig(TIM3);
	
    /* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 250;
	TIM_TimeBaseStructure.TIM_Prescaler = 0; //8MHz clock 32KHz PWM
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);


	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	//TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	//TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	//TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset; 



	 /* PWM1 Mode configuration: Channel4 */	
	TIM_OCInitStructure.TIM_Pulse =0x80; // 50% duty cycle
	TIM_OC4Init(TIM3, &TIM_OCInitStructure);
	// enable preload feature CCR4 shadow registers, copy shadow register to CCR4 when an update event occurs
	//TIM3->CCMR2 |= TIM_CCMR2_OC4PE;
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM3, ENABLE);



	/* TIM3 enable counter */
	//TIM_Cmd(TIM3, ENABLE);
	
	/* TIM3 Main Output Enable */
	TIM_CtrlPWMOutputs(TIM3, ENABLE);
	

	TIM_ClearITPendingBit(TIM3, TIM_EventSource_Update); // clear UIF flag
	
	//enable TIM3 interrupt
	//NVIC_InitTypeDef nvicStructure;
	//nvicStructure.NVIC_IRQChannel = TIM3_IRQn;
	//nvicStructure.NVIC_IRQChannelPriority = 0;
	//nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	//NVIC_Init(&nvicStructure);
	NVIC_EnableIRQ(TIM3_IRQn);
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

}

void TIM3_IRQHandler(void)
{
	TIM_ClearITPendingBit(TIM3,TIM_EventSource_Update); // clear UIF flag

	if (ITcounter < PWMscaler)  // 8kHz data 32kHz pwm if PWMscaler=3
		ITcounter++;
	else
	{
		TIM3->CCR4 = WAVdata;	// write next sample		
		WAV_DataPending = 1;	
		ITcounter = 0;
	}
	
}